/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
// LHCb
// Det/VPDet
#include "VPDet/DeVP.h"
#include "Event/VPFullCluster.h"

/** @class VPOccupancyTuple VPOccupancyTuple.h
 *
 *
 */

class VPOccupancyTuple : public Gaudi::Functional::Consumer<void (const std::vector<LHCb::VPFullCluster> &),
                         Gaudi::Functional::Traits::BaseClass_t< GaudiTupleAlg > > {
public:
  /// Standard constructor
  VPOccupancyTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer(name, pSvcLocator,
               KeyValue("InputLocation",{LHCb::VPFullClusterLocation::Default} ) )
  {}
  

  StatusCode initialize() override; ///< Algorithm initialization
  void operator()(const std::vector<LHCb::VPFullCluster> & clusters) const override;
  

private:
  Gaudi::Property<unsigned int> m_module{this, "Module",0,"Module to consider"};
  Gaudi::Property<std::string> m_tupleName{"TupleName","VPTuple","Ntuple name"};
  std::string  m_clusterLocation;
  DeVP*        m_det = nullptr;
  mutable Gaudi::Accumulators::Counter<unsigned long long> m_nEvt{this,"nEvt"};

};



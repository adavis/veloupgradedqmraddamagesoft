/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LHCb
// Kernel/LHCbKernel
#include "Kernel/VPConstants.h"
// Event/DigiEvent
//#include "Event/VPFullCluster.h"
#include "GaudiAlg/Tuple.h"

// Local
#include "VPOccupancyTuple.h"

DECLARE_COMPONENT( VPOccupancyTuple )

//=============================================================================
// Initialization
//=============================================================================
StatusCode VPOccupancyTuple::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize();
  if ( sc.isFailure() ) return sc;
  // Set histo dir
  //setTupleTopDir( "VP/" );
  // Get detector element
  m_det = getDet<DeVP>( DeVPLocation::Default );

  info()<<"correctly initialized"<<endmsg;
  
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void VPOccupancyTuple::operator()(const std::vector<LHCb::VPFullCluster> & clusters) const 
{

  //info()<<"start"<<endmsg;
    
  //two modules per sensor.

 
  if(clusters.size()==0){
    info()<<"Didn't find clusters!!!"<<endmsg;
  }
  //info()<<"form tuple"<<endmsg;
  
    
  //Take code from Mark, reproduce plots that way 

  int SPAddress[52][98304] = {0}; //module: sensorSP
  
  //int SPAddress_chip[24][8192] = {0}; //chip (within single station 7): chipSP
  int NumClusters[60] = {0};
  int NumPixels[60] = {0};
  //info()<<"loop on clusters"<<endmsg;
  
  for ( LHCb::VPFullCluster cluster : clusters ) {
    
    NumClusters[cluster.channelID().module()]++;
    // Check if the cluster is on the station we are interested in.
    LHCb::VPChannelID id = cluster.channelID();
    if(id.module() != m_module.value())continue;
    
    const DeVPSensor* sensor = m_det->sensor(id);
    //if(sensor->station()!= m_station.value())continue;
    if(sensor->module() != m_module.value())continue;
    //numclusterse.at(cluster.channelID().module()) +=1;
    Tuple cluster_tuple = nTuple( m_tupleName.value()+"_cluster_Module"+std::to_string(m_module.value()) );
    cluster_tuple->column("event",m_nEvt.value());
    cluster_tuple->column("x",cluster.x());
    cluster_tuple->column("y",cluster.y());
    cluster_tuple->column("z",cluster.z());
    cluster_tuple->column("npixels",cluster.pixels().size());
    cluster_tuple->column("channelid_module",id.module());
    cluster_tuple->column("channelid_station",id.station());
    cluster_tuple->column("channelid_channelID",id.channelID());
    cluster_tuple->column("channelid_chip",id.chip());
    cluster_tuple->column("channelid_sensor", id.sensor());
    cluster_tuple->column("channelid_scol",id.scol());
    cluster_tuple->column("channelid_col",id.col());
    cluster_tuple->column("channelid_row",id.row());
    cluster_tuple->write();
    
    
    
    
    for( auto pixel : cluster.pixels()){
      Tuple pixel_tuple = nTuple( m_tupleName.value()+"_pixel_Module"+std::to_string(m_module.value()) );
      
      Gaudi::XYZPoint point = sensor->channelToPoint(pixel, false);
      
      int stationID = pixel.station();
      int module = pixel.module();
      int sensorID = (pixel.sensor() - 4*module);  //from 0-3
      int chip = pixel.chip();
      int col = pixel.col();
      int row = pixel.row();
      int sensorCol = col + 256*chip;
      int SpCol = sensorCol/2;
      int SpRow = row/4;
      int SpNum = SpCol + 384*SpRow;  //384 Superpixels in a row (along full sensor)
      int sensorSP = SpNum + 24576*sensorID;   //24576 Superpixels on a sensor
      int chip24 = (module%2)*12 + sensorID*3 + chip;  //chips ordered from 0-23 within a station
      int chipSP = col/2 + 128*(row/4);

      pixel_tuple->column("event",m_nEvt.value());   
      pixel_tuple->column("stationID",stationID);
      pixel_tuple->column("module",module);
      pixel_tuple->column("sensorID",sensorID);
      
      pixel_tuple->column("chip",chip);
      pixel_tuple->column("col",col);
      pixel_tuple->column("row",row);
      pixel_tuple->column("sensorCol",sensorCol);
      pixel_tuple->column("SpCol",SpCol);
      pixel_tuple->column("SpRow",SpRow);
      pixel_tuple->column("SpNum",SpNum);
      pixel_tuple->column("sensorSP",sensorSP);
      pixel_tuple->column("chip24",chip24);
      pixel_tuple->column("chipSP",chipSP);
      pixel_tuple->column("x",point.x());
      pixel_tuple->column("y",point.y());
      pixel_tuple->column("z",point.z());
      
      pixel_tuple->write();
      
      //info()<<"stationID"<<stationID<<"module"<<module<<"sensorID"<<sensorID<<"chip"<<chip<<"col"<<col<<"row"<<row<<endmsg;
      SPAddress[module][sensorSP]++;
      NumPixels[module]++;
      //numpixels.at(module)+=1;
      
    }
    
  }
  
  
      
  // int NumSPs[60] = {0};  //number of superpixel packets per module per event
  // for(int i=0; i<52; i++){
  //   for(int j=0; j<98304; j++){
  //     if(SPAddress[i][j] >= 1) NumSPs[i]++; 
  //   }
  //   info()<<"filling module" <<i<<endmsg;
    
  //   int numbits = NumSPs[i]*32 + 96;
  //   float numframes = numbits / 256 + 1;
  //   float datarate = numframes * 256 * 40 / 1000.; //rate is in MHz (40), so divide by 1000 to get GB/s
  //   info()<<"datarate = "<<datarate<<endmsg;
    
  //   //numpixels.at(i)=NumPixels[i];
  //   //numclusters.at(i)=NumClusters[i];
  //   //numsuperpixels.at(i)=NumSPs[i];
  //   //NumFrames.at(i)=numframes;
  //   //datarates.at(i)=datarate;
  // }
  
  // module_tuple->farray("NumClusters",numclusters,"module",52);
  // module_tuple->farray("NumSuperPixels",numsuperpixels,"module",52);
  // module_tuple->farray("NumFrames",NumFrames,"module",52);
  // module_tuple->farray("datarate",datarates,"module",52);
    
  //module_tuple->write();
  //info()<<"size of numpixels = "<<numpixels.size()<<", content"<<numpixels<<endmsg;
  
  //info()<<"on event "<<m_nEvt.value()<<endmsg;
  
  m_nEvt++;
  return;
  
}
